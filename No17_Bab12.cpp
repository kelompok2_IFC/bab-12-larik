#include <iostream>
#include <iomanip>
using namespace std;

struct mhs{
	char NIM[11];
	char NAMA[30];
	float IPK;
};
typedef mhs mhsarr[100];

int main(){
	mhsarr x;
	bool keluar = false;
	int menu, i =0, z = 0, y;
	float maks;

	while (keluar==false){
	cout<<"\nMENU UTAMA";
	cout<<"\n1. Input Data Mahasiswa";
	cout<<"\n2. Tampil Data Mahasiswa";
	cout<<"\n3. Tampil Data IPK > 2";
	cout<<"\n4. Tampil Data IPK Tertinggi";
	cout<<"\n5. Keluar";
	
	cout<<"\nMasukkan pilihan anda :";
	cin>>menu;
	
	if(menu==1){
		cout<<"\nNIM : ";
		cin>>x[z].NIM;
		
		cout<<"NAMA : ";
		cin>>x[z].NAMA;
		
		cout<<"IPK : ";
		cin>>x[z].IPK;
		cout<<endl;
		
		z++;
	}	
	else if(menu==2){
		cout<<"\nDaftar Mahasiswa";
		cout<<"\n=========================================";
		cout<<"\n|"<<setw(13)<<"NIM |";
		cout<<setw(32)<<"NAMA |"<<setw(7)<<"IPK |"<<endl;
		cout<<"===========================================\n";
		for(int i=0; i<z;i++){
			cout<<"|"<<setw(11)<<x[i].NIM<<" |";
			cout<<setw(30)<<x[i].NAMA<<" |";
			cout<<setw(5)<<x[i].IPK<<" |"<<endl;
		}
		cout<<"===========================================\n";
	}
	else if(menu==3){
		cout<<"\n IPK > 2";
		cout<<"\n=========================================";
		cout<<"\n|"<<setw(13)<<"NIM |";
		cout<<setw(32)<<"NAMA |"<<setw(7)<<"IPK |"<<endl;
		cout<<"===========================================\n";
		
		for(int i=0; i<z; i++){
			if(x[i].IPK > 2){
				cout<<x[i].IPK<<" > 2";
			}
			cout<<"|"<<setw(11)<<x[y].NIM<<" |";
			cout<<setw(30)<<x[y].NAMA<<" |";
			cout<<setw(5)<<x[y].IPK<<" |"<<endl;
			cout<<"========================================\n";
		}
	}	
	else if(menu==4){
		cout<<"\nIPK Tertinggi";
		cout<<"\n========================================";
		cout<<"\n|"<<setw(13)<<"NIM |";
		cout<<setw(32)<<"NAMA |"<<setw(7)<<"IPK |"<<endl;
		cout<<"==========================================\n";
		
		for(int i=0; i<z; i++){
			if(x[i].IPK>maks){
				maks=x[i].IPK;
				y=i;
			}
			cout<<"|"<<setw(11)<<x[y].NIM<<" |";
			cout<<setw(30)<<x[y].NAMA<<" |";
			cout<<setw(5)<<x[y].IPK<<" |"<<endl;
			cout<<"========================================\n";
		}
	}
	else if(menu==5){
			keluar=true;
			cout<<"TERIMA KASIH\n";
		}
	}
	system("pause");
	}


