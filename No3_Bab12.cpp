//Soal
//No 3. Diberikan larik integer A yang berukuran N elemen. Larik A sudah terdefinisi elemen – elemennya.
//		Tuliskan prosedur yang luarannya adalah elemen terbesar pertama dan elemen terbesar kedua di dalam larik A.

#include <iostream>
using namespace std;

int main()
{
	int n,a[99],b1,b2;
	cout << "Masukan jumlah array : "; cin >>n;
	cout << "*Note : Nilai ke-n harus lebih besar dari nilai pertama" <<endl;
	for (int c=1;c<=n;c++)
	{
		cout << "a["<<c<< "] = "; cin >>a[c];
	}
	b1=a[1];
	
	for (int c=2;c<=n;c++)
	{
		if (a[c]>b1)
		{
			b1=a[c];
		}
	}
	b2=a[1];
	
	for (int c=2;c<=n;c++)
	{
		if ((a[c]>b2)&&(a[c]!=b1))
		{
			b2=a[c];
		}
	}
	
	cout << "Nilai terbesar ke-1 : " <<b1<<endl;
	cout << "Nilai terbesar ke-2 : " <<b2<<endl;
	return 0;
}