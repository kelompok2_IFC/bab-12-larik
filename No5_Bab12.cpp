//Soal
//No 5.	Diberikan larik integer A dan integer larik B yang masing – masing berukuran N elemen. Larik A dan B sudah terdefinisi elemen – elemennya.
//		Tuliskan prosedur untuk mempertukarkan elemen larik A dan elemen B pada indeks yang bersesuaian,
//		sedemikian sehingga larik A berisi elemen – elemen larik B semula dan larik B berisi elemen – elemen larik A semula.

#include <iostream>
using namespace std;

int main()
{
	int na,nb,a[99],b[99],x[99],y[99];
	cout << "Masukan jumlah array : "; cin >>na;
	for (int i=0;i<na;i++)
	{
		cout << "a["<<i<< "] = "; cin >>a[i];
		
		x[i]=a[i];
	}
	
	cout << "\nMasukan jumlah array : "; cin >>nb;
	for (int i=0;i<nb;i++)
	{
		cout << "b["<<i<< "] = "; cin >>b[i];
		
		y[i]=b[i];
	}
	cout << "-----------------------" <<endl;
	for (int i=0;i<nb;i++)
	{
		cout << "Nilai elemen a["<<i<< "] : " <<y[i]<<endl;
	}
	cout << "-----------------------" <<endl;
	for (int i=0;i<na;i++)
	{
		cout << "Nilai elemen b["<<i<< "] : " <<x[i]<<endl;
	}
	return 0;
}