#include <iostream>
using namespace std;
main(){
 int a[4] = {1,2,3,4}; //array yg akan digeser
 int temp; //berfungsi untuk menyimpan dari array a[0]
 for(int i=0;i<4;i++){
  if(i < 1){  //jika i kurang dari 1
   //agar data a[0] masih ada maka disimpan di variabel temp
   temp = a[0];
   a[i] = a[i+1]; //berfungsi agar nilai var a[0] berganti dengan a[1]
  } else if(i >0 && i<3){
   a[i] = a[i+1]; 
  } else if(i == 3){
   a[i] = temp; 
  }
 }

 cout<<"setelah digeser : ";

 for(int i=0;i<4;i++){ //menampilkan hasil pergeseran :
  cout<<" "<<a[i]<<" ";
 }
}

